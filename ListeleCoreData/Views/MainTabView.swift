//
//  MainTabView.swift
//  ListeleCoreData
//
//  Created by Onur on 21.06.2022.
//

import SwiftUI

struct MainTabView: View {
    @Environment(\.colorScheme) var colorScheme
    @State var selectionItem : Int = 0
    @State var isEdit : Bool = false
    @StateObject var vm = CoreDataViewModel()
    var body: some View {
            TabView(selection: $selectionItem) {
                MainListView()
                    .tabItem {
                        Image(systemName: "house.fill")
                        Text("Ana Sayfa")
                    }.tag(0)
                InfoView()
                    .tabItem {
                        Image(systemName: "info")
                        Text("Hakkında")
                    }.tag(1)
            }
            .onAppear() {
                //on appear
            }
            .navigationBarItems(
                leading: NavigationLink(destination: {
                    ProfileView()
                        .navigationTitle("Profil")
                }, label: {
                    HStack {
                        Image(systemName: "person.fill")
                            .accentColor(colorScheme == .dark ? Color.white : Color.black)
                    }
                }),
                trailing: NavigationLink(destination: {
                    SettingsView()
                        .navigationTitle("Ayarlar")
                }, label: {
                    HStack {
                        Image(systemName: "gearshape.fill")
                            .accentColor(colorScheme == .dark ? Color.white : Color.black)
                    }
                })
            )
            .navigationTitle(selectionItem == 0 ? "Listele 📝" : "Info ℹ️")
    }

}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            MainTabView()
        }
        
            
    }
}
