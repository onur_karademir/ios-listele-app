//
//  ProfileView.swift
//  ListeleCoreData
//
//  Created by Onur on 29.06.2022.
//

import SwiftUI

struct ProfileView: View {
    @AppStorage("name") var currentUserName: String?
    @AppStorage("email") var currentUserEmail: String?
    @AppStorage("signed_in") var currentUserSignedIn : Bool = false
    @AppStorage("indexTrophy") var indexTrophy : Int = 0
    @AppStorage("point") var point : Int = 0
    var body: some View {
        VStack {
            VStack (spacing: 15){
                Image(systemName: "person.circle.fill")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 150, height: 150)
                    .foregroundColor(Color.mint)
                VStack {
                    Text("İsim / Nick")
                        .fontWeight(.semibold)
                    Text(currentUserName ?? "your name here")
                        .foregroundColor(Color.mint)
                }
                VStack {
                    Text("Email")
                        .fontWeight(.semibold)
                    Text(currentUserEmail ?? "email ?")
                        .foregroundColor(Color.mint)
                }
                HStack {
                    Text("Puan:")
                        .font(.title3)
                        .fontWeight(.semibold)
                        .foregroundColor(Color.mint)
                    Text("\(point)")
                        .font(.title3)
                        .fontWeight(.semibold)
                        .foregroundColor(
                            point <= 50 ?
                            Color.red : point <= 100 ? Color.green : Color.yellow
                        )
                    Text("/ Level - \(indexTrophy)")
                        .font(.title3)
                        .fontWeight(.semibold)
                        .foregroundColor(Color.mint)
                }
                Text("SIGN OUT")
                    .font(.headline)
                    .foregroundColor(Color.white)
                    .frame(height: 55)
                    .frame(maxWidth: .infinity)
                    .background(Color.mint)
                    .cornerRadius(10)
                    .onTapGesture {
                        //sign out
                        signOut()
                    }
            }
            .font(.title)
            .padding()
            .padding()
            .background(Color.white)
            .cornerRadius(20)
            .shadow(radius: 20)
            Spacer()
        }
        .padding()
    }
    func signOut() {
        currentUserName = nil
        currentUserEmail = nil
        withAnimation(.spring()) {
            currentUserSignedIn = false
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
