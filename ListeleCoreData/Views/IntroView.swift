//
//  IntroView.swift
//  ListeleCoreData
//
//  Created by Onur on 29.06.2022.
//

import SwiftUI

struct IntroView: View {
    @AppStorage("signed_in") var currentUserSignedIn : Bool = false
    var body: some View {
        ZStack {
            //if user signed in
            
            //MainTab View
            
            //else
            
            //onboarding view
            
            if currentUserSignedIn {
                MainTabView()
            } else {
                OnboardingView()
            }
        }
    }
}

struct IntroView_Previews: PreviewProvider {
    static var previews: some View {
        IntroView()
    }
}
