//
//  InfoView.swift
//  ListeleCoreData
//
//  Created by Onur on 21.06.2022.
//

import SwiftUI

struct InfoView: View {
    var body: some View {
        ScrollView {
            VStack (spacing: 20){
                HStack (spacing: 10){
                    Circle()
                        .frame(width: 70, height: 70)
                    VStack (alignment: .leading, spacing: 10){
                        Text("Onur Karademir")
                            .font(.title)
                            .fontWeight(.bold)
                        Text("iletişim: ctos_34@hotmail.com")
                            .font(.callout)
                            .fontWeight(.semibold)
                    }
                    Spacer()
                }
                .frame(maxWidth: .infinity)
                
                HStack {
                    Text("Bu uygulama tamamen ucretsizdir. Eğer benimle;  soru, görüş ve önerileriniz icin iletişime geçmek isterseniz yukarida bulunan mail adresini kullanabilirsiniz. İndirdriğiniz için teşekkurler. 😊")
                        .multilineTextAlignment(.leading)
                        .font(.system(size: 20, weight: .regular, design: .default))
                }
                .padding(.horizontal)
                
               
            }
            //.background(Color.red)
        }
        .padding()
        .frame(maxWidth: .infinity)
    }
}

struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            InfoView()
        }
    }
}
