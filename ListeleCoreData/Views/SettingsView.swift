//
//  SettingsView.swift
//  ListeleCoreData
//
//  Created by Onur on 21.06.2022.
//

import SwiftUI

struct SettingsView: View {
    @AppStorage("font") var fontBold : Bool = false
    @AppStorage("color") var fontColor : Bool = false

    var body: some View {
        ScrollView {
            VStack (spacing: 20){
//                Text("Ayarlar")
//                    .font(.largeTitle)
//                    .fontWeight(.bold)
                VStack (alignment: .leading, spacing: 30){
                    Text("Yazi boyutu")
                        .font(.title2)
                    HStack {
                        Toggle(isOn: $fontBold) {
                            Text("Semple Font")
                                .font(fontBold ? .title : .title2)
                        }
                        //stil degisikligi -- normali yesil
                        .toggleStyle(SwitchToggleStyle(tint: Color.green))
                        Spacer()
                    }
                    .padding(.horizontal, 40)
                    Divider()
                }
                .padding()
                
                VStack (alignment: .leading, spacing: 30){
                    Text("Yazi rengi")
                        .font(.title2)
                    HStack {
                        Toggle(isOn: $fontColor) {
                            Text("Semple Font")
                                .font(.title2)
                                .foregroundColor(fontColor ? Color.yellow : nil)
                        }
                        //stil degisikligi -- normali yesil
                        .toggleStyle(SwitchToggleStyle(tint: Color.green))
                        Spacer()
                    }
                    .padding(.horizontal, 40)
                    Divider()
                }
                .padding(.horizontal)
               
            }
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
            .preferredColorScheme(.dark)
    }
}
