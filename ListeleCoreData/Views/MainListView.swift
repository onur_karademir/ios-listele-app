//
//  MainListView.swift
//  ListeleCoreData
//
//  Created by Onur on 20.06.2022.
//

import SwiftUI
//core data
import CoreData
//import user notif
import UserNotifications
//location
import CoreLocation

//notif
class NotifMenager {
    static let instance = NotifMenager()
    var title : String = ""
    var subTitle : String = ""
    //izin alma
    func requestAuth() {
        let options : UNAuthorizationOptions = [.sound, .alert, .badge]
        UNUserNotificationCenter.current().requestAuthorization(options: options) { succes, error in
            if let error = error {
                print("error, \(error)")
            }else{
                print("succes")
            }
        }
    }
    func sendWeekNotif(title: String, subTitle: String) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.subtitle = subTitle
        content.sound = .default
        content.badge = 1
        
        var dateComponents = DateComponents()
        dateComponents.hour = 12
        dateComponents.minute = 00
        dateComponents.weekday = 1 // hafta
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        let request = UNNotificationRequest(
            identifier: UUID().uuidString,
            content: content,
            trigger: trigger)
        
        UNUserNotificationCenter.current().add(request)
    }
    
    func sendNotif(title: String, subTitle: String) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.subtitle = subTitle
        content.sound = .default
        content.badge = 1
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15.0, repeats: false)
        
        let request = UNNotificationRequest(
            identifier: UUID().uuidString,
            content: content,
            trigger: trigger)
        
        UNUserNotificationCenter.current().add(request)
    }
    
    func cancelNotif() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
}

class CoreDataViewModel : ObservableObject {
    //app point var
    @AppStorage("point") var point : Int = 0
    let container : NSPersistentContainer
    
    @Published var saveEntities : [TodoEntity] = []
    
    @Published var doneFilteredArray : [TodoEntity]  = []
    
    let userDefaultsDone = UserDefaults.standard
    @Published var doneData : [String]  = []
    let userDefaultsUndone = UserDefaults.standard
    @Published var unDoneData : [String]  = []
    
    
    init () {
        container = NSPersistentContainer(name: "TodosContainer")//<=dosya ismi
        container.loadPersistentStores { description, error in
            if let error = error {
                print("error loading core data ! \(error)")
            }
        }
        fatchTodos()
        //done
        doneData = userDefaultsDone.stringArray(forKey: "userDefaultsDone") ?? []
        //undone
        unDoneData = userDefaultsUndone.stringArray(forKey: "userDefaultsUndone") ?? []
    }
    func fatchTodos () {
        let request = NSFetchRequest<TodoEntity>(entityName: "TodoEntity")
        do {
            saveEntities = try container.viewContext.fetch(request)
        }catch let error {
            print("error \(error)")
        }
    }
    func addTodos (todo: String) {
        let newTodo = TodoEntity(context: container.viewContext)
        newTodo.todo = todo
        unDoneData.append(todo)
        userDefaultsUndone.set(unDoneData, forKey: "userDefaultsUndone")
        saveData()
    }
    func saveData () {
        do {
            try container.viewContext.save()
            fatchTodos()
        }catch let error{
            print("error \(error)")
        }
    }
    func deleteItem (indexset: IndexSet) {
        guard let index = indexset.first else {return}
        let todo = saveEntities[index]
        container.viewContext.delete(todo)
        
        if let doneIndexEl = doneData.firstIndex(of: todo.todo ?? "") {
            doneData.remove(at: doneIndexEl)
            userDefaultsDone.set(doneData, forKey: "userDefaultsDone")
        }
        
        if let unDoneIndexEl = unDoneData.firstIndex(of: todo.todo ?? "empty") {
            unDoneData.remove(at: unDoneIndexEl)
            userDefaultsUndone.set(unDoneData, forKey: "userDefaultsUndone")
        }
        
        saveData()
    }
    
    func updateItem (todo: TodoEntity) {
        todo.isComp.toggle()
        doneFilteredArray = saveEntities.filter({$0.isComp})
        
        saveData()
        ptsControl(todo: todo)
        
        if todo.isComp {
            //done
            doneData.append(todo.todo ?? "")
            userDefaultsDone.set(doneData, forKey: "userDefaultsDone")
            if let undoneIndexEl = unDoneData.firstIndex(of: todo.todo ?? "empty") {
                //undone
                unDoneData.remove(at: undoneIndexEl)
                userDefaultsUndone.set(unDoneData, forKey: "userDefaultsUndone")
            }
        } else {
            if let indexEl = doneData.firstIndex(of: todo.todo ?? "") {
                //done
                doneData.remove(at: indexEl)
                userDefaultsDone.set(doneData, forKey: "userDefaultsDone")
                //undone
                unDoneData.append(todo.todo ?? "")
                userDefaultsUndone.set(unDoneData, forKey: "userDefaultsUndone")
            }
        }
    }
    
    func ptsControl(todo: TodoEntity){
        if todo.isComp {
            point += 5
        }else {
            point -= 5
        }
    }
    
    func doneFilteredTodo(){
        doneFilteredArray = saveEntities.filter({$0.isComp})
        saveData()
    }
    
}

struct MainListView: View {
    init () {
        //SegmentedControl style
        UISegmentedControl.appearance().selectedSegmentTintColor = .systemBlue
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.systemBlue], for: .normal)
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
    }
    @StateObject var vm = CoreDataViewModel()
    //todo states
    @State var title: String = ""
    @State var textFieldText : String = ""
    // alert states
    @State var alertTitle :  String = ""
    @State var showAlert : Bool = false
    //app settings
    @AppStorage("font") var fontBold: Bool = false
    @AppStorage("color") var fontColor : Bool = false
    @AppStorage("point") var point : Int = 0
    //SegmentedControl selected section var
    @State var selectionIndex : selectedTabEnum = .one
    //sheets var
    @State var openSheets: Bool = false
    var body: some View {
        VStack(spacing: 20) {
            HStack {
                Text("Puan:")
                    .font(.title)
                    .fontWeight(.semibold)
                    .foregroundColor(Color.mint)
                Text("\(point)")
                    .font(.title)
                    .fontWeight(.semibold)
                    .foregroundColor(
                        point <= 50 ?
                        Color.red : point <= 100 ? Color.green : Color.yellow
                    )
                PtsQuestionButtonview(openSheets: $openSheets)
                
                Spacer()
            }
            TextField("Listeye ekle...", text: $textFieldText)
                .frame(height: 55)
                .padding(.horizontal)
                .background(Color.gray.opacity(0.2))
                .cornerRadius(10)
            //add button
            Button {
                //                        guard !textFieldText.isEmpty else {return}
                //                        vm.addTodos(todo: textFieldText)
                //                        textFieldText = ""
                if textLengtControle() {
                    withAnimation(.default){
                        vm.addTodos(todo: textFieldText)
                        textFieldText = ""
                    }
                    NotifMenager.instance.sendNotif(title: "Yeni ve zorlu bir gorevin var.😁", subTitle: "Başarılar dileriz.💪🏻")
                }
            } label: {
                Text("Ekle 🥰" .uppercased())
                    .fontWeight(.bold)
                    .frame(height: 55)
                    .frame(maxWidth: .infinity)
                    .background(Color.mint)
                    .foregroundColor(Color.white)
                    .cornerRadius(10)
                
            }
            VStack {
                Picker("select", selection: $selectionIndex) {
                    ForEach(selectedTabEnum.allCases, id: \.self) { item in
                        Image(systemName: "\(item.rawValue)").tag(item)
                    }
                }
                .padding(.top, 4)
                .pickerStyle(SegmentedPickerStyle())
            }
            switch selectionIndex {
            case .one:
                if vm.saveEntities.isEmpty {
                    
                    VStack {
                        Text("Ana liste 🤩")
                            .font(.subheadline)
                            .fontWeight(.semibold)
                        List {
                            withAnimation (.default){
                                Text("Tum liste bos 😲")
                                    .font(.subheadline)
                                    .fontWeight(.semibold)
                            }
                        }
                        .onAppear() {
                            NotifMenager.instance.sendWeekNotif(title: "Hafta başladı 🚀", subTitle: "Listene yapacaklarını eklemeyi unutma 😇")
                        }
                        .listStyle(PlainListStyle())
                    }
                } else {
                    VStack {
                        Text("Ana liste 🥸")
                            .font(.subheadline)
                            .fontWeight(.semibold)
                        List {
                            ForEach(vm.saveEntities) { item in
                                HStack {
                                    Image(systemName: item.isComp ? "checkmark.circle" : "circle")
                                        .font(fontBold ? .title : .title2)
                                        .foregroundColor(item.isComp ? Color.green : Color.red)
                                    
                                    if item.isComp {
                                        Text(item.todo ?? "empty todo")
                                            .foregroundColor(Color.gray)
                                            .font(fontBold ? .title : .title2)
                                            .fontWeight(.medium)
                                            .italic()
                                            .strikethrough()
                                    } else {
                                        Text(item.todo ?? "empty todo")
                                            .foregroundColor(fontColor ? Color.yellow : nil)
                                            .font(fontBold ? .title : .title2)
                                            .fontWeight(.medium)
                                    }
                                    
                                }
                                .onTapGesture {
                                    vm.updateItem(todo: item)
                                }
                            }
                            .onDelete { IndexSet in
                                vm.deleteItem(indexset: IndexSet)
                                
                                NotifMenager.instance.sendNotif(title: "Tebrikler görev tamamlandı 🥰", subTitle: "Hayatına düzen getirirken bizi kullandığın için teşekkürler 🎉 🌏")
                            }
                            .onMove { IndexSet, Int in
                                vm.saveEntities.move(fromOffsets: IndexSet, toOffset: Int)
                            }
                        }
                        .padding(.bottom)
                        .listStyle(PlainListStyle())
                    }
                }
                
            case .two:
                if vm.doneData.isEmpty {
                    VStack {
                        Text("Tamamlananlar 🥸")
                            .font(.subheadline)
                            .fontWeight(.semibold)
                        List {
                            withAnimation (.default){
                                Text("Tamamlandi listesi bos 😎")
                                    .font(.subheadline)
                                    .fontWeight(.semibold)
                            }
                        }
                        .listStyle(PlainListStyle())
                    }
                } else {
                    VStack {
                        Text("Tamamlananlar 🥸")
                        Text("Ogeleri kaldirmak icin ana listeye gidiniz 🥳")
                            .font(.subheadline)
                            .fontWeight(.semibold)
                        List {
                            ForEach(vm.doneData, id:\.self) { item in
                                HStack {
                                    Image(systemName: "checkmark.circle")
                                        .foregroundColor(Color.green)
                                    Text(item)
                                        .foregroundColor(Color.gray)
                                        .font(fontBold ? .title : .title2)
                                        .fontWeight(.medium)
                                        .italic()
                                        .strikethrough()
                                }
                            }
                        }
                        .padding(.bottom)
                        .listStyle(PlainListStyle())
                    }
                }
                
            case .three:
                if vm.unDoneData.isEmpty {
                    VStack {
                        Text("Tamamlanmayanlar 🥺")
                            .font(.subheadline)
                            .fontWeight(.semibold)
                        List {
                            withAnimation (.default){
                                Text("Işler yolunda mı? 🥺")
                                    .font(.subheadline)
                                    .fontWeight(.semibold)
                            }
                        }
                        .listStyle(PlainListStyle())
                    }
                } else {
                    VStack {
                        Text("Tamamlanmayanlar 🥺")
                            .font(.subheadline)
                            .fontWeight(.semibold)
                        Text("Öğeleri kaldirmak icin ana listeye gidiniz 🥳")
                            .font(.subheadline)
                            .fontWeight(.semibold)
                        List {
                            ForEach(vm.unDoneData, id:\.self) { item in
                                HStack {
                                    Image(systemName: "circle")
                                        .foregroundColor(Color.red)
                                    Text(item)
                                        .foregroundColor(Color.gray)
                                        .font(fontBold ? .title : .title2)
                                        .fontWeight(.medium)
                                }
                            }
                        }
                        .padding(.bottom)
                        .listStyle(PlainListStyle())
                    }
                }
            }
            
        }
        .padding(.horizontal)
        //.navigationTitle("Listele 📝")
        .alert(isPresented: $showAlert) {
            Alert(title: Text(alertTitle))
        }
        .onAppear() {
            NotifMenager.instance.requestAuth()
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
        .sheet(isPresented: $openSheets) {
            MultipleSheetsView()
        }
    }
    func textLengtControle() -> Bool {
        if textFieldText.count < 3 {
            alertTitle = "Yapolacaklar listesine eklenebilmesi icin en az 3 karakter giriniz. 😨"
            showAlert = true
            return false
        }
        return true
    }
    
}
enum selectedTabEnum: String, CaseIterable {
    case one = "list.triangle"
    case two = "checkmark.diamond.fill"
    case three = "exclamationmark.arrow.circlepath"
}

struct ChoseListView : View {
    var selectedListView : selectedTabEnum
    
    var body: some View {
        switch selectedListView {
        case .one:
            Text("All")
        case .two:
            Text("Done")
        case .three:
            Text("Undone")
        }
    }
}


struct MainListView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            MainListView()
        }
    }
}

struct PtsQuestionButtonview: View {
    @Binding var openSheets: Bool
    var body: some View {
        Button {
            openSheets.toggle()
        } label: {
            Image(systemName: "questionmark.circle")
                .font(.title2)
                .foregroundColor(Color.accentColor)
        }
    }
}
