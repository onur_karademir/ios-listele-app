//
//  OnboardingView.swift
//  ListeleCoreData
//
//  Created by Onur on 29.06.2022.
//

import SwiftUI

struct OnboardingView: View {
    
    //page state
    @State var onbordingState : Int = 0
    @State var name : String = ""
    @State var email : String = ""
    //appstorage data
    @AppStorage("name") var currentUserName: String?
    @AppStorage("email") var currentUserEmail: String?
    @AppStorage("signed_in") var currentUserSignedIn : Bool = false
    //alert state
    @State var alertTitle: String = ""
    @State var showAlert: Bool = false
    //animation add custom
    let transition : AnyTransition = .asymmetric(insertion: .move(edge: .trailing), removal: .move(edge: .leading))
    
    var body: some View {
        ZStack {
            
            ZStack {
                RadialGradient(
                    gradient: Gradient(colors: [Color.mint, Color.purple]),
                    center: .topLeading,
                    startRadius: 5,
                    endRadius: UIScreen.main.bounds.height)
                .ignoresSafeArea()
                switch onbordingState {
                case 0:
                    //welcome section
                    welcomeSection
                        .transition(transition)
                case 1:
                    //about section
                    aboutSection
                        .transition(transition)
                case 2:
                    addNameSection
                        .transition(transition)
                case 3:
                    addEmailSection
                        .transition(transition)
                case 4:
                    finalSection
                        .transition(transition)
                default:
                    RoundedRectangle(cornerRadius: 25)
                        .foregroundColor(.yellow)
                        .frame(height: 300)
                }
                //buttons
                VStack {
                    Spacer()
                    bottomButton
                }
                .padding(30)
            }
            .alert(isPresented: $showAlert) {
                return Alert(title: Text(alertTitle))
            }
        }
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView()
            .background(Color.purple)
    }
}

extension OnboardingView {
    //button content
    private var bottomButton: some View {
        Text(onbordingState == 3 ? "BİTİR" : "SONRAKİ")
            .font(.headline)
            .foregroundColor(.purple)
            .frame(height: 55)
            .frame(maxWidth: .infinity)
            .background(Color.white)
            .cornerRadius(10)
            //.animation(nil) // animasyondan muhaf tutma
        //click events
            .onTapGesture {
                hendleNextButtonPressed()
            }
    }
    
    //case 0 welcome section
    private var welcomeSection: some View {
        VStack (spacing: 40){
            //push the bottom
            Spacer()
            Image(systemName: "heart.text.square.fill")
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200)
                .foregroundColor(.white)
            Text("Hoş Geldin")
                .underline()
                .font(.largeTitle)
                .fontWeight(.semibold)
                .foregroundColor(Color.white)
            Text("Listele'yi kullnamadan once kisa bir tur atmaya ne dersin 😉")
                .foregroundColor(Color.white)
                .fontWeight(.medium)
                .multilineTextAlignment(.center)
            //push the top
            Spacer()
            Spacer()
        }
        //all stack padding
        .padding(20)
    }
    
    //case 1 welcome section
    private var aboutSection: some View {
        VStack (spacing: 40){
            //push the bottom
            Spacer()
            Image(systemName: "gear.badge.questionmark")
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200)
                .foregroundColor(.white)
            Text("Listele Hakkinda")
                .underline()
                .font(.largeTitle)
                .fontWeight(.semibold)
                .foregroundColor(Color.white)
            Text("Listele'yi kullnırken gun icinde yapacaklarını ekleyebilir, cikartabilir ve tamamlayabilirsin. Bunlari yaparken puanlar toplayip yeni seviyeleri acabilirsin. Hem eğlenip hemde hayatına duzen getirmeye ne dersin?")
                .foregroundColor(Color.white)
                .fontWeight(.medium)
                .multilineTextAlignment(.center)
            //push the top
            Spacer()
            Spacer()
        }
        //all stack padding
        .padding(20)
    }
    
    // case 2
    //name textfield section
    private var addNameSection: some View {
        VStack (spacing: 20){
            //push the bottom
            Spacer()
          
            Text("İsim ya da nick girebilirsin")
                .font(.title)
                .fontWeight(.semibold)
                .foregroundColor(Color.white)
                .underline()
            
            TextField("İsim ya da nick...", text: $name)
                .font(.headline)
                .frame(height: 55)
                .padding(.horizontal)
                .background(Color.white)
                .cornerRadius(20)
            //push the top
            Spacer()
            Spacer()
        }
        //all stack padding
        .padding(20)
    }
    
    // case 3
    //name textfield section
    private var addEmailSection: some View {
        VStack (spacing: 20){
            //push the bottom
            Spacer()
          
            Text("Email adresini girebilirsin")
                .font(.title)
                .fontWeight(.semibold)
                .foregroundColor(Color.white)
                .underline()
            
            TextField("email", text: $email)
                .keyboardType(.emailAddress)
                .font(.headline)
                .frame(height: 55)
                .padding(.horizontal)
                .background(Color.white)
                .cornerRadius(20)
            //push the top
            Spacer()
            Spacer()
        }
        //all stack padding
        .padding(20)
    }
    
    //case 4 welcome section
    private var finalSection: some View {
        VStack (spacing: 40){
            //push the bottom
            Spacer()
            Image(systemName: "brain.head.profile")
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200)
                .foregroundColor(.white)
            Text("Tebrikler 🥳🤩")
                .underline()
                .font(.largeTitle)
                .fontWeight(.semibold)
                .foregroundColor(Color.white)
            Text("Artik Listele'nin keyfini cıkartmaya baslayabilirirsin.")
                .foregroundColor(Color.white)
                .fontWeight(.medium)
                .multilineTextAlignment(.center)
            //push the top
            Spacer()
            Spacer()
        }
        //all stack padding
        .padding(20)
    }
    
    func hendleNextButtonPressed() {
        // check inputs
        switch onbordingState {
        case 2:
            guard name.count >= 3 else {
                alertTitle = "Isim ya da nick en az 3 karakterden olusmaidir ☺️"
                showAlert.toggle()
                return
            }
        case 3:
            guard email.contains("@") else {
                alertTitle = "Email adresi olduguna emin misin? 🧐"
                showAlert.toggle()
                return
            }
        default:
            break
        }
        if onbordingState == 4 {
            //sign in
            signIn()
        } else {
            withAnimation(.spring()) {
                onbordingState += 1
            }
        }
    }
    
    func signIn() {
        currentUserName = name
        currentUserEmail = email
        withAnimation(.spring()) {
            currentUserSignedIn = true
        }
    }


}
